#!/bin/bash
#SBATCH -N 4
#SBATCH -p GPU
#SBATCH --ntasks-per-node 4
#SBATCH --gres=gpu:4
#SBATCH -t 5
#SBATCH --reservation=IHPCSS

mpirun -n 16 ./a.out

