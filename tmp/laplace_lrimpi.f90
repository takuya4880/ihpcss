!*************************************************
! Laplace MPI Fortran Version
!
! Temperature is initially 0.0
! Boundaries are as follows:
!
!          T=0.   
!       _________                    ____|____|____|____  
!       |       | 0                 |    |    |    |    | 
!       |       |                   |    |    |    |    | 
! T=0.  | T=0.0 | T                 | 0  | 1  | 2  | 3  | 
!       |       |                   |    |    |    |    | 
!       |_______| 100               |    |    |    |    | 
!       0     100                   |____|____|____|____|
!                                        |    |    |
! Each Processor works on a sub grid and then sends its
! boundaries to neighbours
!
!  John Urbanic, PSC 2014
!
!*************************************************
program mpi
      use accel_lib
      implicit none

      include    'mpif.h'

      !Size of plate
      integer, parameter             :: columns_global=10000
      integer, parameter             :: rows_global=10000

      !these are the new parameters for parallel purposes
      integer, parameter             :: xpes=4
      integer, parameter             :: ypes=4
      integer, parameter             :: total_pes=xpes*ypes
      integer, parameter             :: columns=columns_global/total_pes
      integer, parameter             :: rows=rows_global/total_pes
      integer, parameter             :: left=100, right=101, up=102, down=103
      integer, parameter             :: outfile=20

      !usual mpi variables
      integer                        :: mype, npes, ierr
      integer                        :: myxpe, myype
      integer                        :: status(MPI_STATUS_SIZE)

      double precision, parameter    :: max_temp_error=0.01

      integer                        :: i, j, iteration=1
      integer, parameter             :: max_iterations=4000
      double precision               :: dt, dt_global=100.0
      real                           :: start_time, stop_time

      !variables for hybrid
      integer                        :: nd, mydevice, req(8)


      double precision, dimension(0:rows+1,0:columns+1) :: temperature, temperature_last
      double precision, dimension(0:columns+1) :: sendbufu, sendbufd, recvbufu, recvbufd

      !usual mpi startup routines
      call MPI_Init(ierr)
      call MPI_Comm_size(MPI_COMM_WORLD, npes, ierr)
      call MPI_Comm_rank(MPI_COMM_WORLD, mype, ierr)

      myxpe = mod(mype,xpes) 
      myype = mype/ypes
         
      if( mype == 0 ) then
         open(outfile,file='output.txt',status='replace')
      endif

      nd = acc_get_num_devices(acc_device_nvidia)
      print*, nd
      mydevice = mod(mype,nd)
      call acc_set_device_num(mydevice,acc_device_nvidia)
      nd = acc_get_num_devices(acc_device_nvidia)
      !It is nice to verify that proper number of PEs are running
      if ( npes /= total_pes ) then
         if( mype == 0 ) then
            write (outfile,*)'This example is hardwired to run only on ', total_pes, ' PEs'
         endif
         call MPI_Finalize(ierr)
         stop
      endif

      !Only one PE should prompt user
      if( mype == 0 ) then
         !print*, 'Maximum iterations [100-4000]?'
         !read*,   max_iterations
         write (outfile,*) 'Maximum iterations is ', max_iterations
      endif
      
      !Other PEs need to recieve this information
      call MPI_Bcast(max_iterations, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)

      call cpu_time(start_time)

      call initialize(temperature_last, npes, mype)

      !do until global error is minimal or until maximum steps
      !$acc data copy(temperature_last), create(temperature)
      do while ( dt_global > max_temp_error .and. iteration <= max_iterations)

         !$acc kernels
         do j=1,columns
            do i=1,rows
               temperature(i,j)=0.25*(temperature_last(i+1,j)+temperature_last(i-1,j)+ &
                                      temperature_last(i,j+1)+temperature_last(i,j-1) )
            enddo
         enddo
         !$acc end kernels

         ! COMMUNICATION PHASE
         !$acc update host(temperature(:,1),temperature(:,columns),temperature(1,:),temperature(rows,:))
         ! send messages to right
         if (myxpe < xpes-1) then
            call MPI_Isend(temperature(1,columns), rows, MPI_DOUBLE_PRECISION, &
                          mype+1, RIGHT, MPI_COMM_WORLD, req(1),ierr)
         endif
         if (myxpe /= 0) then
            call MPI_Irecv(temperature_last(1,0), rows, MPI_DOUBLE_PRECISION, &
                          MPI_ANY_SOURCE, RIGHT, MPI_COMM_WORLD, req(2), ierr)
         endif

         ! send messages to left
         if (myxpe /= 0) then
            call MPI_Isend(temperature(1,1), rows, MPI_DOUBLE_PRECISION, &
                          mype-1, LEFT, MPI_COMM_WORLD, req(3), ierr)
         endif
         if (myxpe /= xpes-1) then
            call MPI_Irecv(temperature_last(1,columns+1), rows, MPI_DOUBLE_PRECISION, &
                          MPI_ANY_SOURCE, LEFT, MPI_COMM_WORLD, req(4), ierr)
         endif 
         
         ! send messages to up
         if (myype < ypes-1) then
            sendbufu = temperature(1,:)
            call MPI_Isend(sendbufu, columns+2, MPI_DOUBLE_PRECISION, &
                          mype+xpes, UP, MPI_COMM_WORLD, req(5),ierr)
         endif
         if (myype /= 0) then
            call MPI_Irecv(recvbufu, columns+2, MPI_DOUBLE_PRECISION, &
                          MPI_ANY_SOURCE, UP, MPI_COMM_WORLD, req(6), ierr)
            temperature_last(0,:) = recvbufu
         endif
         
         ! send messages to down
         if (myype < ypes-1) then
            sendbufd = temperature(rows,:)
            call MPI_Isend(sendbufd, columns+2, MPI_DOUBLE_PRECISION, &
                          mype+xpes, UP, MPI_COMM_WORLD, req(7),ierr)
         endif
         if (myype /= 0) then
            call MPI_Irecv(recvbufd, columns+2, MPI_DOUBLE_PRECISION, &
                          MPI_ANY_SOURCE, UP, MPI_COMM_WORLD, req(8), ierr)
            temperature_last(rows+1,:) = recvbufd
         endif
         !$acc update device(temperature_last(:,0),temperature_last(:,columns+1),temperature_last(0,:),temperature_last(rows+1,:))

         dt=0.0

         !$acc kernels
         do j=1,columns
            do i=1,rows
               dt = max( abs(temperature(i,j) - temperature_last(i,j)), dt )
               temperature_last(i,j) = temperature(i,j)
            enddo
         enddo
         !$acc end kernels


         !Need to determine and communicate maximum error
         call MPI_Reduce(dt, dt_global, 1, MPI_DOUBLE_PRECISION, MPI_MAX, 0, MPI_COMM_WORLD, ierr)
         call MPI_Bcast(dt_global, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
         
         call MPI_WaitAll(8,req,status)
         !periodically print test values - only for PE in lower corner
         if( mod(iteration,500).eq.0 ) then
            if( mype == npes-1 ) then
               !!$acc update host(temperature)
               !$acc update host(temperature(rows-5:rows,columns-5:columns))
               call track_progress(temperature, iteration)
            endif
         endif

         iteration = iteration+1

      enddo
      !$acc end data


      !Slightly more accurate timing and cleaner output
      call MPI_Barrier(MPI_COMM_WORLD, ierr)

      call cpu_time(stop_time)

      if( mype == 0 ) then
         write (outfile,*) 'Max error at iteration ', iteration-1, ' was ',dt_global
         write (outfile,*) 'Total time was ',stop_time-start_time, ' seconds.'
      endif
      if( mype == 2) then
         print*, 'magic point', temperature(9950,2500)
      endif

      call MPI_Finalize(ierr)

end program mpi

!Parallel version requires more attention to global coordinates
subroutine initialize(temperature_last, npes, mype )
      implicit none

      integer, parameter             :: columns_global=10000
      integer, parameter             :: rows=10000
      integer, parameter             :: total_pes=16
      integer, parameter             :: columns=columns_global/total_pes

      integer                        :: i,j
      integer                        :: npes,mype

      double precision, dimension(0:rows+1,0:columns+1) :: temperature_last
      double precision               :: tmin, tmax

      temperature_last = 0

      !Left and Right Boundaries
      if( mype == 0 ) then
         do i=0,rows+1
            temperature_last(i,0) = 0.0
         enddo
      endif
      if( mype == npes-1 ) then
         do i=0,rows+1
            temperature_last(i,columns+1) = (100.0/rows) * i
         enddo
      endif

      !Top and Bottom Boundaries
      tmin =  mype    * 100.0/npes
      tmax = (mype+1) * 100.0/npes
      do j=0,columns+1
         temperature_last(0,j) = 0.0
         temperature_last(rows+1,j) = tmin + ((tmax-tmin)/columns) * j
      enddo

end subroutine initialize

subroutine track_progress(temperature, iteration)
      implicit none

      integer, parameter             :: columns_global=10000
      integer, parameter             :: rows=10000
      integer, parameter             :: total_pes=16
      integer, parameter             :: columns=columns_global/total_pes

      integer                        :: i,iteration

      double precision, dimension(0:rows+1,0:columns+1) :: temperature

!Parallel version uses global coordinate output so users don't need
!to understand decomposition
      write (outfile,*) '---------- Iteration number: ', iteration, ' ---------------'
      do i=5,0,-1
         write (outfile,'("("i5,",",i5,"):",f6.2,"  ")',advance='no') &
                   rows-i,columns_global-i,temperature(rows-i,columns-i)
      enddo
      write (outfile,*)
end subroutine track_progress
