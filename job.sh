#!/bin/bash
#SBATCH -N 4
#SBATCH -p GPU
#SBATCH --ntasks-per-node 4
#SBATCH --gres=gpu:4
#SBATCH -t 1
#SBATCH --reservation=IHPCSS

#export PGI_ACC_TIME=1
mpirun -n 16 ./a.out

